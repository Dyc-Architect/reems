from django.urls import path
from infoapp import views


app_name = 'info'
urlpatterns = [
    path('home/',views.home,name='home'),
    # path('add/',views.add,name='add'),
    path('add_logic/',views.add_logic,name='add_logic'),
    path('delete/',views.delete,name='delete'),
    path('update/',views.update,name='update'),
    path('update/',views.update,name='update'),
    path('load_update/',views.load_update,name='update'),
    path('update_logic/',views.update_logic,name='update_logic'),
    path('fun/',views.fun,name='fun'),
]