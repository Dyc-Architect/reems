from django.db import models


# Create your models here.
class Info(models.Model):
    name = models.CharField(max_length=20)
    salary = models.DecimalField(max_digits=7,decimal_places=2)
    age = models.SmallIntegerField()
    head = models.ImageField(upload_to='pics')