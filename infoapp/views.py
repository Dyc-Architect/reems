import os
import uuid

from django.core.paginator import Paginator
from django.db import transaction
from django.http import JsonResponse
from django.shortcuts import render, redirect,HttpResponse
from django.urls import reverse

from infoapp.models import Info


# Create your views here.
def user_default(u):
    if isinstance(u,Info):
        return {'id':u.id,'name':u.name,'age':u.age,'salary':str(u.salary),'head':str(u.head)}


def home(request):
    login = request.session.get('login')
    if login:
        info = list(Info.objects.all())
        return JsonResponse({'info':info},json_dumps_params={"default":user_default})
    return HttpResponse('×')


# def add(request):
#     return render(request, 'info/../static/addEmp.html')


def generateUUID(headname):
    id = str(uuid.uuid4())
    extend = os.path.splitext(headname)[1]
    return id+extend


def add_logic(request):
    try:
        name = request.POST.get("name")
        salary = request.POST.get("salary")
        age = request.POST.get("age")
        # print(name,age,salary)
        headpic = request.FILES.get("picture")
        if headpic:
            ext = os.path.splitext(headpic.name)
            headpic.name = str(uuid.uuid4()) + ext[1]
        with transaction.atomic():
            # print(name,salary,age,headpic)
            Info.objects.create(name=name, age=age, salary=salary, head=headpic)
            return HttpResponse('√')
    except:
        return HttpResponse('×')


def delete(request):
    id = request.GET.get("id")
    # print(id)
    emp = Info.objects.get(id=id)
    if emp:
        emp.delete()
        return HttpResponse(1)


def update(request):
    id = request.GET.get("id")
    ems = Info.objects.get(id=id)
    # print(id)
    if ems:
        request.session["updateId"] = id
        return HttpResponse(1)
    return HttpResponse(0)


def load_update(request):
    param = request.GET.get("param")
    # print(param)
    id = request.session.get(param)
    info = Info.objects.get(id=id)
    return JsonResponse({'info':info},safe=False,json_dumps_params={'default':user_default})


def fun(request):
    return HttpResponse('1')

def update_logic(request):
    # print('dfg')
    try:
        with transaction.atomic():
            id = request.POST.get("eId")
            print(id)
            ems = Info.objects.get(id=id)
            ems.name = request.POST.get("name")
            ems.salary = request.POST.get("salary")
            ems.age = request.POST.get("age")
            headpic = request.FILES.get("headpic")
            print(id,ems.name,ems.salary,ems.age,headpic)
            if headpic:
                ext = os.path.splitext(headpic.name)
                headpic.name = str(uuid.uuid4()) + ext[1]
                ems.head = headpic
                # picture()
                print(id, ems.name, ems.salary, ems.age, headpic)
                ems.save()
            return HttpResponse('1')
    except:
        return HttpResponse(0)
