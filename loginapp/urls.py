from django.urls import path
from loginapp import views
app_name = 'login'

urlpatterns = [
    path('login/', views.login, name='login'),
    path('login_logic/', views.login_logic, name='login_logic'),
    # path('register/', views.register, name='register'),
    path('register_logic/', views.register_logic, name='register_logic'),
    path('getcaptcha/', views.getcaptcha, name='captcha'),
    path('check/', views.check, name='check'),
    path('check_capt/', views.check_capt, name='check_capt'),
]