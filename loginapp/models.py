from django.db import models


# Create your models here.
from django.shortcuts import render, redirect
from django.utils.deprecation import MiddlewareMixin


class User(models.Model):
    name = models.CharField(max_length=20)
    password = models.CharField(max_length=20)
    real_name = models.CharField(max_length=20)
    sex = models.CharField(max_length=20)

    class Meta:
        db_table = 'user'


class MyMiddleware(MiddlewareMixin):
    def __init__(self, get_response):
        super().__init__(get_response)
        # print("init1")

    def process_request(self, request):
        if "login" not in request.path:
            # print("登录验证")
            session = request.session
            if session.get("login"):
                print("已登录")
            else:
                # print("未登录")
                return redirect('login:login')
        else:
            print("正在登录")

#     def process_view(self, request, view_func, view_args, view_kwargs):
#         print("view:", request, view_func, view_args, view_kwargs)
#
#     def process_response(self, request, response):
#         print("response:", request, response)
#         return response
#
#     def process_exception(self, request, ex):
#         print("exception:", request, ex)
