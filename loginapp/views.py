import os
import uuid

from django.db import transaction
from django.shortcuts import render, HttpResponse, redirect
from loginapp.models import User
import random, string
from loginapp.captcha.image import ImageCaptcha


# Create your views here.
def login(request):
    name = request.COOKIES.get('username')
    pwd = request.COOKIES.get('password')
    user = User.objects.filter(name=name, password=pwd)
    if user:
        request.session['login'] = 'ok'
        return redirect('info:home')
    return render(request, 'login/login.html')


def login_logic(request):
    print('hhhh')
    name = request.POST.get('name')
    pwd = request.POST.get('pwd')
    rem = request.POST.get('remember')
    user = User.objects.filter(name=name, password=pwd)
    if user:
        respond = HttpResponse('√')
        if rem:
            respond.set_cookie('username', name, max_age=7 * 24 * 3600)
            respond.set_cookie('password', pwd, max_age=7 * 24 * 3600)
        request.session['login'] = 'ok'
        return respond
    else:
        return HttpResponse('×')


# def register(request):
#     return render(request, 'login/regist.html')


def generateUUID(headname):
    id = str(uuid.uuid4())
    extend = os.path.splitext(headname)[1]
    return id + extend


def register_logic(request):
    try:
        name = request.POST.get('username')
        real_name = request.POST.get('name')
        pwd = request.POST.get('pwd')
        sex = request.POST.get('sex')
        print(name, real_name, pwd, sex)
        with transaction.atomic():
            User.objects.create(name=name, real_name=real_name, password=pwd, sex=sex)
        return HttpResponse('√')
    except:
        return HttpResponse('×')


def check(request):
    print('dfgsdf')
    name = request.GET.get('name')
    print(name)
    result = User.objects.filter(name=name)
    print(result)
    if result:
        return HttpResponse('×')
    else:
        return HttpResponse('√')


def getcaptcha(request):
    image = ImageCaptcha(fonts=[os.path.abspath('loginapp/captcha/FZZJ-KFTXKJW.TTF')])
    code = random.sample(string.ascii_letters + string.digits, 4)
    random_code = ''.join(code)
    request.session['code'] = random_code
    print(random_code.lower())
    data = image.generate(random_code)
    return HttpResponse(data, 'image/png')


def check_capt(request):
    capt = request.GET.get('number')
    print(capt)
    capt2 = request.session.get('code').lower()
    print(capt2)
    if capt == capt2:
        return HttpResponse('√')
    else:
        return HttpResponse('×')
